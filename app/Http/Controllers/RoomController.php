<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\MessageBag;
use App\Models\Room;

use App\Http\Requests;

class RoomController extends Controller
{
    public function index()
    {
        if (Session::has('room_name') && Session::has('login_user_name')) {
            $room_name = Session::get('room_name');
            $login_user_name = Session::get('login_user_name').' さん：';
        } else {
            $room_name = '選択されていません';
            $login_user_name = '';
        }
        return view('create_room.index', compact('room_name', 'login_user_name'));
    }

    public function store(Request $request)
    {
        // バリデーションチェック
        $validator = $this->validate($request, [
            'room_name' => 'required|max:255|unique:rooms',
            'password' => 'required|min:4|confirmed',
        ]);

        $messages = new MessageBag;

        try {
            Room::createRoom($request);
            $messages->add('success', '作成しました');
        } catch (\Exception $e) {
            $messages->add('error', 'エラーが発生しました');
            Log::debug($e);
        }
        
        return redirect('create_room/index')->withErrors($messages);
    }
}
