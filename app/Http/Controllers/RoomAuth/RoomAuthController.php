<?php

namespace App\Http\Controllers\RoomAuth;

use App\Models\Room;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class RoomAuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    // プロパティ設定
    protected $guard = 'room';
    protected $redirectTo = '/program';
    protected $loginView = 'login.room_login';
    protected $redirectAfterLogout = '/room_login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * リクエストの処理
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function postRoomLogin(Request $request)
    {
        $this->validate($request, [
            'room_name' => 'required|max:255',
            'password' => 'required',
            'login_user_name' => 'required|max:255',
        ]);
        $throttles = $this->isUsingThrottlesLoginsTrait();
    
        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
    
        $credentials = $request->only('room_name', 'password');
    
        if (Auth::guard('room')->attempt($credentials, $request->has('remember'))) {
            session()->put('room_name', $request->room_name);
            session()->put('login_user_name', $request->login_user_name);
            session()->put('user_role', 1);
            return redirect()->intended($this->redirectPath());
        }
    
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }
    
        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'room_name' => 'required|max:255',
            'password' => 'required',
            'login_user_name' => 'required|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'room_name' => $data['room_name'],
            'password' => $data['password'],
            'login_user_name' => 'required|max:255',
        ]);
    }
}
