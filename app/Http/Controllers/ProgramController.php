<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\MessageBag;

class ProgramController extends Controller
{
    /**
     * Scratch画面
     *
     * @return string
     */
     public function index()
     {
        if (Session::has('room_name') && Session::has('login_user_name')) {
            $room_name = Session::get('room_name');
            $login_user_name = Session::get('login_user_name').' さん：';
        } else {
            // エラーとしてログアウトさせる
        }
        return view('program.program', compact('room_name', 'login_user_name'));
     }

     /**
     * ファイルアップロード
     *
     * @return string
     */
    public function store(Request $request)
    {
        // アップロードパスを指定する。(/public/upload)
        $upload_file_path = public_path().'/upload/';

        // バリデーションチェック
        $validator = $this->validate($request, [
            'file' => 'required',
        ]);

        // ファイル保存
        $result = $request->file('file')->isValid();
        if ($result) {
            // ファイルを格納する。
            $file = $request->file('file')->move($upload_file_path , "test.json");
        }
        $messages = new MessageBag;
        $messages->add('success', 'アップロード完了しました');
        
        return redirect('program')->withErrors($messages);
    }
}
