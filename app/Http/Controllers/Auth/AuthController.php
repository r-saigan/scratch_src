<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $guard = 'web';
    protected $redirectTo = '/create_room/index';
    protected $loginView = 'login.login';
    protected $redirectAfterLogout = '/create_room/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * リクエストの処理
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
     public function postCreateRoomLogin(Request $request)
     {
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);
         $throttles = $this->isUsingThrottlesLoginsTrait();
     
         if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
             $this->fireLockoutEvent($request);
             return $this->sendLockoutResponse($request);
         }
     
         $credentials = $request->only('email', 'password');
     
         if (Auth::guard('web')->attempt($credentials, $request->has('remember'))) {
            return redirect()->intended($this->redirectPath());
         }
     
         if ($throttles && ! $lockedOut) {
             $this->incrementLoginAttempts($request);
         }
     
         return $this->sendFailedLoginResponse($request);
     }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
