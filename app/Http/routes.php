<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => ''], function() {
    // ルームログイン画面
    Route::get('room_login', 'RoomAuth\RoomAuthController@showLoginForm');
    Route::post('room_login', 'RoomAuth\RoomAuthController@postRoomLogin');
    Route::get('room_logout', 'RoomAuth\RoomAuthController@logout');

    // ルーム作成ログイン画面
    Route::get('create_room/login', 'Auth\AuthController@showLoginForm');
    Route::post('create_room/login', 'Auth\AuthController@postCreateRoomLogin');
    Route::get('create_room/logout', 'Auth\AuthController@logout');

    // Scratch
    Route::group(['middleware' => ['auth:room']], function() {
        // Scratch画面
        Route::get('program', 'ProgramController@index');
        // ポストで受け取った際に処理
        Route::post('program/store', 'ProgramController@store');
    });

    // ルーム作成
    Route::group(['middleware' => ['auth:web']], function() {
        // ルーム作成画面
        Route::get('create_room/index', 'RoomController@index');
        // ポストで受け取った際に処理
        Route::post('create_room/store', 'RoomController@store');
    });
});
