<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class Room extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'room_name', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * ルーム新規作成
     *
     * @param Reqest $request
     * @return void
     */
    public static function createRoom($request) {
        DB::transaction(function() use($request) {
            //インスタンス生成
            $room = new Room;
            $room->room_name = $request->room_name;
            $room->password = bcrypt($request->password);
            //保存
            $room->save();
        });
    }
}
