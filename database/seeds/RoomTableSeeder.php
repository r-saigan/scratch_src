<?php

use Illuminate\Database\Seeder;

class RoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rooms')->insert([
            'id' => 1,
            'room_name' => 'room1',
            'password' => bcrypt('test1234'),
        ]);
    }
}
