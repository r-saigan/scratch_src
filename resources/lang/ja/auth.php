<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '入力された情報に間違いがあります。もう一度入力してください。',
    'throttle' => 'ただいまシステムが混み合っています。 しばらく待ってから実行してください。',

];
