<!DOCTYPE html>
<html>
@yield('header')

@yield('sidebar')

<div class="content-wrapper">
@yield('content')
</div>

@yield('footer')
</div>
</body>
</html>