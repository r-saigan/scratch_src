@section('sidebar')
<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">Scratch</li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-fw fa-laptop"></i> <span>Scratch</span>
        <span class="pull-right-container">
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="/program"><i class="fa fa-circle-o"></i> Scratch</a></li>
        <li><a href="#"><i class="fa fa-circle-o"></i> ファイル送信</a></li>
      </ul>
    </li>
    <li class="header">Management</li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-fw fa-gear"></i> <span>Management</span>
        <span class="pull-right-container">
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="/create_room/index"><i class="fa fa-circle-o"></i> ルーム管理</a></li>
        <li><a href="#"><i class="fa fa-circle-o"></i> 管理ページ</a></li>
      </ul>
    </li>
  </ul>
</section>
<!-- /.sidebar -->
</aside>
@endsection