@section('footer')
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> α
  </div>
  <strong>Copyright © SoftBank Group All rights reserved.
</footer>
@endsection