@extends('layouts.base')

@include('layouts.header')
@include('layouts.sidebar')
@include('layouts.footer')

@section('content')
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">ルーム作成</h3>
            </div>
            @if (count($errors) > 0)
                @if ($errors->has('success'))
                    <div class="alert alert-success">
                        {{ $errors->first('success') }}
                    </div>
                @else
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            @endif
            <form role="form" action="/create_room/store" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                    <div class="form-group {{ $errors->has('room_name') ? 'has-error' : '' }}">
                        <label for="exampleInputEmail1">ルーム名</label>
                        <input type="text" name="room_name" class="form-control" id="exampleInputEmail1" placeholder="ルーム名" value="{{ old('room_name') }}">
                        @if ($errors->has('room_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('room_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label for="exampleInputPassword1">パスワード</label>
                        <input type="text" name="password" class="form-control" id="exampleInputPassword1" placeholder="パスワード" value="{{ old('password') }}">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                        <label for="exampleInputPassword1">パスワード 確認用</label>
                        <input type="text" name="password_confirmation" class="form-control" id="exampleInputPassword1" placeholder="パスワード 確認用" value="{{ old('password_confirmation') }}">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">作成</button>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
@endsection
