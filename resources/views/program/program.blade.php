@extends('layouts.base')

@include('layouts.header')
@include('layouts.sidebar')
@include('layouts.footer')

@section('content')
@if (count($errors) > 0)
	@if ($errors->has('success'))
	<div class="alert alert-success">
	{{ $errors->first('success') }}
	</div>
	@else
	<div class="alert alert-danger">
		<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
		</ul>
	</div>
	@endif
@endif
<form action="program/store" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
    <input type="file" name="file" size="30">
	@if ($errors->has('file'))
		<span class="help-block">
			<strong>{{ $errors->first('file') }}</strong>
		</span>
	@endif
	</div>
    <button type="submit" onclick="send();">保存</button>
</form>
<div class="modal fade" id="modal_box">
  <div class="modal-dialog modal-sm">

  </div>
</div>
<iframe  id="scratch" src="http://ec2-18-220-195-119.us-east-2.compute.amazonaws.com:3000" name="sample" width="100%" height="1200">
</iframe>

<script>
jQuery(document).ready(function(){
	jQuery('.modal').modal('show');
});
$(window).on('load', function(){
	jQuery(".modal").modal('hide');
});

function send(){
	if (window.confirm('保存してよろしいですか？')) {
		return true;
	} else {
		window.alert('キャンセルされました');
        return false;
	}
}
</script>
@endsection

